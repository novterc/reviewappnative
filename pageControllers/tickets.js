appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;


    self.ticketsPageClass = function(){
        var obj = this;

        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'ticketsPage';
        obj.preInit = function($type){
            if( $type == undefined) $type = 'all';
            App.nG.TicketsCtrl.onlyStatusType = $type;
            App.inTimeout(App.nG.TicketsCtrl.$setScope);
        };
        obj.init = function(){

            App.nG.TicketsCtrl.ticketsCount = function(tickets, type){
                if( tickets == undefined || type == undefined ) return 0;
                if( type == 'all'){
                    return tickets.count();
                } else {
                    var statusName = {'inwork':1, 'aside':3, 'closed':2};

                    var count = 0;
                    var list = tickets.getList();
                    for (var itemKey in list) {
                        if( list[itemKey].status == statusName[type]) count++;
                    }
                    return count;
                }
            };

            App.tuser.company(function(company){
                company.tickets(function(tickets){
                    tickets.listObserver(function(tickets){
                        App.nG.TicketsCtrl.tickets = tickets;
                        App.inTimeout(App.nG.TicketsCtrl.$setScope);
                    });
                });
            });
            //App.inTimeout(App.nG.TicketsCtrl.$setScope);

        };
        return this;
    };
    self.ticketsPage = new self.ticketsPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.ticketPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'ticketPage';
        obj.$destroy = true;
        obj.init = function($id){

            var company = App.tuser.company(function(company){
                company.tickets(function(tickets){

                    var tickit = tickets.find($id);

                    //tickit.clientChannel(function(clientChannel) {
                    //    App.view.pageTitle(clientChannel.notify+' - '+clientChannel.number);
                    //});


                    App.nG.TicketCtrl.chnagePriority = function(ticket, priority){
                        ticket.priority = (priority?2:1);
                        ticket.save();
                    };

                    tickit.addObserver(function(tickit){
                        App.nG.TicketCtrl.ticket = tickit;
                        App.nG.TicketCtrl.priority = (tickit.priority == '2'?true:false);
                        App.inTimeout(App.nG.TicketCtrl.$setScope);
                    });

                    tickit.messages(function(messages){
                        App.nG.TicketCtrl.messages = messages;
                        App.inTimeout(App.nG.TicketCtrl.$setScope);
                    });

                    App.nG.TicketCtrl.ticketGoClientContact = function(tickit){
                        tickit.clientChannel(function(clientChannel){
                            if( clientChannel.contact_id != 0 )
                                App.view.openHref('/client/'+clientChannel.contact_id);
                            else
                                App.view.openHref('/contact/'+tickit.client_channel_id);
                        });
                    };

                    App.nG.TicketCtrl.tickerAnswerSubmit = function(newMessage){
                        var newMessageData = App.getScopeAttr(newMessage);
                        App.resetScopeAttr(newMessage);
                        newMessageData.type = 1;
                        newMessageData.type_message = 1;
                        tickit.createMessage(newMessageData);
                    };

                    App.nG.TicketCtrl.ticketCloseButtonClick = function(tickit){
                        if(confirm('Закрыть тикет?') ){
                            App.view.openHref('/tickets');
                            tickit.close();
                        }
                    };

                    App.nG.TicketCtrl.ticketDeleteButtonClick = function(tickit){
                        if(confirm('Удалить тикет?') ){
                            App.view.openHref('/tickets/inwork');
                            tickit.delete();
                        }
                    };

                    //App.nG.TicketCtrl.setAside = {};
                    var aaside = moment(tickit.aside, "YYYY-MM-DD").format("MM/DD/YYYY");
                    if( aaside != 'Invalid date' &&  App.Models.getRetryByDate(aaside) > 0 ) {
                        App.nG.TicketCtrl.asideDate = aaside;
                    }else{
                        var tomorrow = new Date();
                        tomorrow.setDate(tomorrow.getDate()+1);
                        var aaside = moment(tomorrow).format("MM/DD/YYYY");
                        App.nG.TicketCtrl.asideDate = aaside;
                    }


                    App.nG.TicketCtrl.setAsideFormSubmit = function(ticket, asideDate){
                        ticket.setAside(moment(asideDate, "MM/DD/YYYY").format("YYYY-MM-DD"));
                        App.view.openHref('/tickets/inwork');
                    };

                    company.quickReplys(function(quickReplys){
                        quickReplys.listObserver(function(quickReplys){
                            App.nG.TicketCtrl.quickReplys = quickReplys;
                            App.inTimeout(App.nG.TicketCtrl.$setScope);
                        });

                        App.nG.TicketCtrl.selectQuickReplys = function(quickReply){
                            if( App.nG.TicketCtrl.newMessage == undefined ) {
                                App.nG.TicketCtrl.newMessage = {};
                                App.nG.TicketCtrl.newMessage.text = quickReply.text;
                            }else
                                App.nG.TicketCtrl.newMessage.text = App.nG.TicketCtrl.newMessage.text+' '+quickReply.text;
                        }
                    })

                });
            });
            obj.addDestroyObj(company);
        };
        return this;
    };
    self.ticketPage= new self.ticketPageClass();


})
    .wait('pageControllers','load')
    .waitingRun();