appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;

    self.channelAddPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'channelAddPage';
        obj.init = function(){

            App.nG.channelAddCtrl.channelAddFormSubmit = function(){
                var attrs = App.getScopeAttr(App.nG.channelAddCtrl);
                var attrsExtend = App.getScopeAttr(App.nG.channelAddCtrl.extend[attrs.channelExtenderType]);
                var attrsAll = App.merge_options(attrs, attrsExtend);
                App.tuser.company(function(company){
                    company.createChannel(attrsAll, function(data){
                        App.log('-= create callBAck in pageController ', data);
                        App.resetScopeAttr(appNative.nG.channelAddCtrl);
                        App.view.openHref('/channel/'+data.id);

                    })
                })
            }

        };
        return this;
    };
    self.channelAddPage = new self.channelAddPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.channelsPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'channelsPage';
        obj.init = function(){

            App.tuser.company(function(company){
                company.channels(function(channels){
                    channels.listObserver(function(channels){
                        App.nG.ChannelsCtrl.channels = channels;
                        App.inTimeout(App.nG.ChannelsCtrl.$setScope);
                    });
                })
            })

        };
        return this;
    };
    self.channelsPage = new self.channelsPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.channelPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'channelPage';
        obj.$destroy = true;
        obj.init = function($id){

            App.nG.ChannelCtrl.channelStatusClient = App.lang.get('channelStatusClient');

            var company = App.tuser.company(function(company){
                company.channels(function(channels){

                    var channel = channels.find($id);

                    channel.addObserver(function(channel){
                        App.nG.ChannelCtrl.channel = channel;
                        App.inTimeout(App.nG.ChannelCtrl.$setScope);
                    });

                    channel.extenderType(function(extenderType){
                        extenderType.addObserver(function(extenderType) {
                            App.nG.ChannelCtrl.extenderType = extenderType;
                            App.inTimeout(App.nG.ChannelCtrl.$setScope);

                            if( channel.type_id == '1') {
                                App.nG.ChannelCtrl.tockenRetrySec = extenderType.getTockenRetrySec();
                                SMSRetryTimer();
                                TockenRetryTimer();
                            }
                        });
                    });

                    App.nG.ChannelCtrl.channelFormSubmit = function(channel, extenderType){


                        channel.save();
                        extenderType.save();
                    };

                    App.nG.ChannelCtrl.channelDelete = function(channel){
                        if(confirm(App.lang.get('channel', 'confirm_delete'))) {
                            App.view.openHref('/channels');
                            channel.delete();
                        }
                    };

                    App.nG.ChannelCtrl.getSmsCodeClick= function(extenderType){
                        extenderType.getSMSCode(function(data){
                            App.log('______====== getSMSCode', data);
                        });
                    };

                    App.nG.ChannelCtrl.getTockenClick= function(extenderType, tockenKey){
                        extenderType.getTocken(tockenKey, function(data){
                            App.log('______====== getTocken', data);
                            App.nG.ChannelCtrl.sms_code =='';
                            App.nG.ChannelCtrl.setToken.$setPristine();
                            App.nG.ChannelCtrl.$setScope();
                        });
                    };
                });
            });
            obj.addDestroyObj(company);
        };
        return this;
    };
    self.channelPage = new self.channelPageClass();


})
    .wait('pageControllers','load')
    .waitingRun();