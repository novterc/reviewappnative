appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;

    self.quickReplyPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'quickReplyPage';
        obj.$destroy = true;
        obj.init = function($id){

            App.tuser.company(function(company){
                company.quickReplys(function(quickReplys){
                    var quickReply = quickReplys.find($id);
                    quickReply.addObserver(function(quickReply){
                        App.nG.QuickReplyCtrl.quickReply = quickReply;
                        App.inTimeout(App.nG.QuickReplyCtrl.$setScope);
                    });

                    App.nG.QuickReplyCtrl.quickReplyFormSubmit = function(quickReply){
                        quickReply.save();
                    };

                    App.nG.QuickReplyCtrl.quickReplyDelete = function(quickReply){
                        if(confirm(App.lang.get('quickReply', 'confirm_delete'))) {
                            App.view.openHref('/quickReplys');
                            quickReply.delete();
                        }
                    };
                });
                obj.addDestroyObj(company);
            })

        };
        return this;
    };
    self.quickReplyPage = new self.quickReplyPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.quickReplysPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'quickReplysPage';
        obj.init = function(){

            App.tuser.company(function(company){
                App.nG.QuickReplysCtrl.quickReplysAddFormSubmit = function(quickReplysAdd){
                    company.createQuickReplys(quickReplysAdd)
                    App.resetScopeAttr(quickReplysAdd);
                };
                company.quickReplys(function(quickReplys){
                    quickReplys.listObserver(function(quickReplys){
                        App.nG.QuickReplysCtrl.quickReplys = quickReplys;
                        App.inTimeout(App.nG.QuickReplysCtrl.$setScope);
                    });
                })
            })

        };
        return this;
    };
    self.quickReplysPage = new self.quickReplysPageClass();


})
    .wait('pageControllers','load')
    .waitingRun();