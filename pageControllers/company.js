appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;

    self.companyPageClass = function(){
        var obj = this;

        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'companyPage';
        obj.init = function(){

            App.tuser.company(function(company){
                company.addObserver(function(company){
                    App.nG.companyCtrl.company = company;
                    App.inTimeout(App.nG.companyCtrl.$setScope);
                });
            });

            App.nG.companyCtrl.comapnyFormSubmit = function(company){
                company.save()
            };

        };
        return this;
    };
    self.companyPage = new self.companyPageClass();

})
    .wait('pageControllers','load')
    .waitingRun();