appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;


    self.contactPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'contactPage';
        obj.$destroy = true;
        obj.init = function($id){

            //App.nG.ChannelCtrl.channelStatusClient = App.lang.get('channelStatusClient');

            var company = App.tuser.company(function(company){
                company.clientChannels(function(clientChannels){

                    var $clientChannels = clientChannels.find($id);

                    $clientChannels.addObserver(function(clientChannel){
                        App.nG.ContactCtrl.contact = clientChannel;
                        App.inTimeout(App.nG.ContactCtrl.$setScope);
                    });

                    App.nG.ContactCtrl.contactSelectClientClick = function(contactId){
                        $clientChannels.contact_id = contactId;
                        $clientChannels.save();
                    };

                    App.nG.ContactCtrl.contactClearClientClick = function($event, clientChannels){
                        $event.preventDefault();
                        clientChannels.contact_id = 0;
                        clientChannels.save();
                    }

                });

                company.contacts(function(contacts){
                    contacts.listObserver(function(contacts){
                        App.nG.ContactCtrl.clients = contacts;
                        App.inTimeout(App.nG.ContactCtrl.$setScope);
                    });
                });


            });
            obj.addDestroyObj(company);
        };
        return this;
    };
    self.contactPage= new self.contactPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.contactsPageClass = function(){
        var obj = this;

        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'contactsPage';
        obj.init = function(){

            App.tuser.company(function(company){
                company.clientChannels(function(clientChannels){
                    clientChannels.listObserver(function(clientChannels){
                        App.nG.ContactsCtrl.contacts = clientChannels;
                        App.inTimeout(App.nG.ContactsCtrl.$setScope);
                    });
                });
            });


        };
        return this;
    };
    self.contactsPage = new self.contactsPageClass();



})
    .wait('pageControllers','load')
    .waitingRun();