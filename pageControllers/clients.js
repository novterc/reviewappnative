appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;

    self.clientAddPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'clientAddPage';
        obj.init = function(){

            App.tuser.company(function(company){
                App.nG.clientAddCtrl.clientAddFormSubmit = function(client){
                    var attrs = App.getScopeAttr(client);
                    attrs.bday = moment(attrs.aday, "DD.MM.YYYY").format("YYYY-MM-DD");
                    if(attrs.name == undefined || attrs.name == '') attrs.name = 'Имя не указано';
                    App.resetScopeAttr(client);
                    App.nG.clientAddCtrl.clientAddForm.$setPristine();
                    App.view.openHref('/clients');
                    company.createContact(attrs);
                };
            });

        };
        return this;
    };
    self.clientAddPage = new self.clientAddPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.clientPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'clientPage';
        obj.$destroy = true;
        obj.init = function($id){

            App.tuser.company(function(company){
                company.contacts(function(contacts){

                    var contact = contacts.find($id);
                    if( contact == undefined ){
                        App.view.tost('Клиент не найден');
                        App.view.openHref('/clients');
                        return false;
                    }

                    contact.addObserver(function(contact){
                        if( contact.bday != '0000-00-00')
                            contact.abday = moment(contact.bday, "YYYY-MM-DD").format("MM/DD/YYYY");
                        else
                            contact.abday = '';
                        App.nG.ClientCtrl.client = contact;
                        App.inTimeout(App.nG.ClientCtrl.$setScope);;
                    });

                    contact.clientChannels(function(clientChannels){
                        clientChannels.listObserver(function(clientChannels){
                            App.nG.ClientCtrl.contacts = clientChannels;
                            App.inTimeout(App.nG.ClientCtrl.$setScope);
                        })
                    });

                    App.nG.ClientCtrl.clientFormSubmit = function(contact, clientChannels){

                        var clientChannelsList = clientChannels.getList();
                        for (var attrname in clientChannelsList) {
                            clientChannelsList[attrname].save();
                        }

                        contact.bday = moment(contact.abday, "MM/DD/YYYY").format("YYYY-MM-DD");
                        if(contact.name == '') contact.name = 'Имя не указано';
                        contact.save();
                    };

                    App.nG.ClientCtrl.clientDelete = function(client){
                        if(confirm(App.lang.get('client', 'confirm_delete'))) {
                            App.view.openHref('/clients');
                            client.delete();
                        }
                    };

                });
                obj.addDestroyObj(company);
            })

        };
        return this;
    };
    self.clientPage = new self.clientPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.clientsPageClass = function(){
        var obj = this;

        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'clientsPage';
        obj.init = function(){

            App.tuser.company(function(company){
                company.contacts(function(contacts){
                    contacts.addObserver(function(contacts){
                        App.nG.ClientsCtrl.clients = contacts;
                        App.inTimeout(App.nG.ClientsCtrl.$setScope);
                    });
                });
            });

        };
        return this;
    };
    self.clientsPage = new self.clientsPageClass();


})
    .wait('pageControllers','load')
    .waitingRun();