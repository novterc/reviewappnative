appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;

    self.profilePageClass = function(){
        var obj = this;

        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'profilePage';
        obj.init = function(){

            App.tuser.addObserver(function(tuser){
                //App.log('----=',tuser.lang_id);
                App.nG.profileCtrl.user = tuser;
                App.inTimeout(App.nG.profileCtrl.$setScope);
            });

            App.nG.profileCtrl.profilePersonalSubmit = function(user){
                user.save()
            };

            App.nG.profileCtrl.ProfilePasswordSubmit = function(chp){
                var attrs = App.getScopeAttr(chp);
                App.resetScopeAttr(chp);
                App.nG.UserCtrl.ProfilePassword.$setPristine();
                App.tuser.changePassword(attrs, function(data){
                    if( data ){
                        App.view.tost("Пароль успешно изменен");
                    }else{
                        App.view.tost("Неверный пароль");
                    }
                });
            };

        };
        return this;
    };
    self.profilePage = new self.profilePageClass();


})
    .wait('pageControllers','load')
    .waitingRun();