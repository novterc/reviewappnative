appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;

    self.indexPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'indexPage';
        obj.init = function(){

            App.tuser.company(function(company){
                company.clientChannels(function(clientChannels){
                    clientChannels.listObserver(function(list){
                        App.nG.indexCtrl.clientChannels = list;
                        App.inTimeout(App.nG.indexCtrl.$setScope);
                    });
                })
            });

        };
        return this;
    };
    self.indexPage = new self.indexPageClass();


})
    .wait('pageControllers','load')
    .waitingRun();