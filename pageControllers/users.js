appNative.createRelation(function(){
    var self = appNative.pageControllers;
    var App = appNative;

    self.usersPageClass = function(){
        var obj = this;

        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'usersPage';
        obj.init = function(){

            App.tuser.company(function(company){
                company.users(function(users){
                    users.listObserver(function(users){
                        App.nG.UsersCtrl.users = users;
                        App.inTimeout(App.nG.UsersCtrl.$setScope);
                    });
                });
            });

        };
        return this;
    };
    self.usersPage = new self.usersPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.userPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'userPage';
        obj.$destroy = true;
        obj.init = function($id){

            var company = App.tuser.company(function(company){
                company.users(function(users){

                    var $user = users.find($id);

                    $user.addObserver(function(user){
                        App.nG.UserCtrl.user = user;
                        App.inTimeout(App.nG.UserCtrl.$setScope);
                    });

                    App.nG.UserCtrl.profilePersonalSubmit = function(user){
                        user.save()
                    };

                    App.nG.UserCtrl.ProfilePasswordSubmit = function(chp){
                        var attrs = App.getScopeAttr(chp);
                        App.resetScopeAttr(chp);
                        App.nG.UserCtrl.ProfilePassword.$setPristine();
                        $user.changePassword(attrs, function(data){
                            if( data ){
                                App.view.tost("Пароль успешно изменен");
                            }else{
                                App.view.tost("Не верный пароль");
                            }
                        });
                    };

                    App.nG.UserCtrl.profilePersonalDelete = function(user){
                        App.view.openHref('/users');
                        user.delete();
                    };

                });
            });
            obj.addDestroyObj(company);

        };
        return this;
    };
    self.userPage= new self.userPageClass();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.userAddPageClass = function(){
        var obj = this;
        obj.__proto__ = new self.PageClass(obj);
        obj.pageName = 'userAddPage';
        obj.init = function(){

            App.tuser.company(function(company){
                App.nG.userAddCtrl.userAddPersonalSubmit = function(newUser){
                    var attrs = App.getScopeAttr(newUser);
                    if(attrs.phone == undefined)  attrs.phone = '';

                    appNative.tuser.company(function(company){
                        company.checkUserEmail(attrs.email, function(checkRes){

                            if( checkRes == 0 ) {
                                App.resetScopeAttr(newUser);
                                App.nG.userAddCtrl.userAddPersonalForm.$setPristine();
                                company.createUser(attrs);
                                App.view.openHref('/users');
                            }else{
                                App.view.tost('EMAIL уже использован');
                            }

                        })
                    });


                };
            });

        };
        return this;
    };
    self.userAddPage = new self.userAddPageClass();

})
    .wait('pageControllers','load')
    .waitingRun();