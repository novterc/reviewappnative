function AppNative(config) {
    var AppNative = this;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    AppNative.log = function () {
        if ( config.debug && window.console && console.log) {
            try {
                console.log.apply(console, arguments)
            } catch (e) {
                console.log(Array.slice(arguments))
            }
        }
    };
    var log = AppNative.log;

    AppNative.dlog = function () {
        try {
            console.log.apply(console, arguments)
        } catch (e) {
            console.log(Array.slice(arguments))
        }
    };

    AppNative.implode = function( glue, pieces ) {
        return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
    };


    function error(code, msg, descObj){
        var e = new Error('dummy');
        log('-----======= ERROR ======-------');
        log('code: '+code+' message: '+msg);
        log(descObj);
        log(e);
        log('-----======= END ======---------');

    }
    AppNative.error = error;


    function getSize(obj){
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    }
    AppNative.getSize = getSize;


    function merge_options(obj1,obj2) {
        var obj3 = {};
        for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
        for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
        return obj3;
    }
    AppNative.merge_options = merge_options;


    AppNative.inTimeout = function(Fn){
        setTimeout(Fn, 0);
    };


    AppNative.getScopeAttr = function(scope){
        var data = {};
        for (var key in scope) {
            if( key.substr(0,1) != '$' && typeof scope[key] == "string" ){
                data[key] = scope[key];
            }
        }
        return data;
    };


    AppNative.resetScopeAttr = function(scope){
        for (var key in scope) {
            if( key.substr(0,1) != '$' && typeof scope[key] == "string" ){
                scope[key] = '';
            }
        };
    };


    AppNative.config = AppNative.merge_options({}, config);

////////////////////////////////////////// download class //////////////////////////////////////////////////////////////

    AppNative.loadClass = function(nameClass, loadPath){
        log('load: '+nameClass+' - '+loadPath);

        var nameClass = nameClass;
        var script = document.createElement('script');
        script.onload = function() {
            AppNative.onLoadClass(nameClass);
        };
        script.src = loadPath;
        document.getElementsByTagName('head')[0].appendChild(script);

    };


    AppNative.onLoadClass = function( nameClass ){
        AppNative.relationModuleSetStatus(nameClass, 'load');
    };


    AppNative.inClass = function(name, callb){
        AppNative[name] = callb(AppNative, {});
    };

///////////////////////////////////////// relation /////////////////////////////////////////////////////////////////////

    AppNative.relationsModules = {};
    AppNative.relationWaites = new Array();


    AppNative.relationModuleSetStatus = function(moduleName, statusName){
        if(AppNative.relationsModules[moduleName]==undefined)
            AppNative.relationsModules[moduleName]={};
        AppNative.relationsModules[moduleName][statusName] = true;
        //log('relation setStat: '+moduleName+' '+statusName);
        relationCheckWait(moduleName, statusName);
    };


    function relationCheckWait (moduleName, statusName){
        AppNative.relationWaites.forEach(function(item) {
            if(typeof item == 'object')
                item.setComplete(moduleName, statusName)
        });
    }


    AppNative.createRelation = function(callback, name){

        var iself = {};
        iself.name = name;
        iself.callback = callback;
        iself.waites = {};
        iself.waitingStatus = false;


        iself.wait = function(moduleName, statusName){
            if( AppNative.relationsModules[moduleName]==undefined || AppNative.relationsModules[moduleName][statusName]==undefined || AppNative.relationsModules[moduleName][statusName] == false){
                if( iself.waites[moduleName]==undefined)
                    iself.waites[moduleName] = {};
                iself.waites[moduleName][statusName] = true;
            }
            return iself;
        };


        iself.waitingRun = function(){
            iself.waitingStatus = true;
            iself.check();
        };


        iself.setComplete = function(moduleName, statusName){
            if(iself.waitingStatus == true) {
                if (iself.waites[moduleName] != undefined && iself.waites[moduleName][statusName] != undefined) {
                    delete iself.waites[moduleName][statusName];
                    if (AppNative.getSize(iself.waites[moduleName]) == 0) {
                        delete iself.waites[moduleName];
                        iself.check();
                    }
                }
            }
        };


        iself.check = function(){
            if(iself.waitingStatus == true) {
                if (AppNative.getSize(iself.waites) == 0) {
                    iself.complete()
                }
            }
        };


        iself.complete = function(){

            self.waitingStatus = false;
            iself.callback();
            AppNative.relationWaites[iself.numberWaites] = true;
            //log('relation Complete: '+name);
        };


        iself.numberWaites = (AppNative.relationWaites.push(iself)-1);

        return iself;
    };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    AppNative.models = {};
    AppNative.data = {};
    AppNative.listLoaded = {};
    AppNative.lists = new Array();
    AppNative.nG = {};


    AppNative.createRelation(function(){
        appNative.webSocketHelper.getObj('$user', {}, function(data){
            AppNative.Models.setDataObj('User', data.id, data);
            AppNative.tuser = new AppNative.Models.UserClass(data);
            AppNative.tuser.$getPath = '$user';
            AppNative.relationModuleSetStatus('ThisUserClass', 'init');


            appNative.tuser.company(function(company){
                appNative.nG.navCtrl.getTicketTypeCount = company.getTicketTypeCount;
                appNative.ccc = company;
            })

        });
    }, 'indexCtrl')
        .wait('webSocketHelper','connect')
        .wait('angular','load')
        .wait('models','init')
        .waitingRun();


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    AppNative.run = function() {
        log('=========== AppNative run ================');

        appNative.loadClass('webSocketHelper', '/js/appNative/classes/WebScoketHelper.js');
        appNative.loadClass('pageControllers', '/js/appNative/classes/PageControllers.js');
        appNative.loadClass('router', '/js/appNative/classes/Router.js');
        appNative.loadClass('view', '/js/appNative/classes/View.js', 'View');
        appNative.loadClass('popupFullScrin', '/js/appNative/classes/PopupFullScrin.js');
        appNative.loadClass('lang', '/js/appNative/classes/Lang.js');
        appNative.loadClass('models', '/js/appNative/classes/Models.js');
        appNative.loadClass('events', '/js/appNative/classes/Events.js');

    };

    return this;
}