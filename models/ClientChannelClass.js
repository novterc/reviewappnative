appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.ClientChannelClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'ClientChannel';
        obj.$attributes = ['company_id', 'contact_id', 'type', 'status', 'notify', 'number', 'name'];


        obj.company = function(callback){
            return obj.belongsTo(self.CompanyClass, '.company', 'company_id', 'id', callback);
        };


        obj.contact = function(callback){
            if( obj.contact_id != '0')
                return obj.belongsTo(self.ContactClass, '.contact', 'contact_id', 'id', callback);

            //App.error(6054,'ClientChannel not can call contact - contact_id is zero', {obj:obj});
        };


        obj.messages = function(callback){
            return obj.hasMany(self.MessageClass, '.messages', 'id', 'client_channel_id', callback)
        };


        obj.countMessages = function(){
            var $messages = obj.getInitObj('messages', obj.messages);
            if( $messages != null)
                return $messages.count();
            else
                return '';
        };


        obj.getNumber = function(){
            return obj.number;
        };


        obj.getTypeName = function(){
            return App.lang.get('ClientChannelTypeName', obj.type);
        };


        obj.getTypeNameClient = function(){
            return App.lang.get('ClientChannelTypeNameClient', obj.type);
        };


        obj.getContactName = function(){
            var $contact = obj.getInitObj('contact', obj.contact, obj.contact_id);
            if( $contact != null)
                return $contact.name;
            else
                return '';
        };


        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();