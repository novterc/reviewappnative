appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.ChannelWhatsappClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'ChannelWhatsapp';
        obj.$attributes = ['channel_id', 'phone', 'nick', 'session_password', 'session_status', 'session_expiration_time', 'kind', 'price', 'cost', 'currency', 'price_expiration_time', 'sms_retry', 'tocken_retry'];


        obj.basicChannel = function(callback){
            return obj.belongsTo(self.ChannelClass, '.basicChannel', 'channel_id', 'id', callback);
        };


        obj.getNumber = function(){
            return obj.phone;
        };


        obj.getNick = function(){
            return obj.nick;
        };


        obj.getStatus = function(){
            return obj.session_status;
        };


        obj.isVerify = function(){
            if( obj.session_status == '4' )
                return true;
            else
                return false;
        };


        obj.getSmsRetrySec = function(){
            return self.getRetryByDate(obj.sms_retry);
        };


        obj.getTockenRetrySec = function(){
            return self.getRetryByDate(obj.tocken_retry);
        };


        obj.getSMSCode = function(callback){
            App.webSocketHelper.getFn(obj.$getPath+'.getSMSCode($attributes)', {$attributes:$attributes}, function (data) {
                if( callback !== undefined) callback(data);
            })
        };


        obj.getTocken = function(tockenKey, callback){
            App.webSocketHelper.getFn(obj.$getPath+'.getTocken($attributes)', {$attributes:tockenKey}, function (data) {
                if( callback !== undefined) callback(data);
            }, true)
        };


        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();