appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.BasicClass = function(obj){
        var obj = obj;
        obj.__proto__ = new self.protoClass(obj);

        obj.$objParent = null;
        obj.id = null;
        obj.inited = false;
        obj.$defattr = ['id', 'created_at', 'updated_at'];


        obj.getObjData = function($classDataName, $getPath, $localKey, $foreignKey, $list, callback){

            if( $list == true && App.listLoaded[$getPath] == true ) {
                var objData = self.searchData($classDataName, $foreignKey, obj[$localKey], $list);
                callback(objData);
                return true;
            }

            if( $list == false ){
                var objData = self.searchData($classDataName, $foreignKey, obj[$localKey], $list);
                if( objData.length != 0 ){
                    callback(objData);
                    return true;
                }
            }

            App.webSocketHelper.getObj($getPath, {}, function (data) {
                //App.log('-getObj-', data);

                if( $list == true ){
                    App.listLoaded[$getPath] = true;

                    data.forEach(function(item){
                        self.setDataObj($classDataName, item.id, item);
                    });

                } else {
                    self.setDataObj($classDataName, data.id, data);
                }
                callback(data);
            });
        };


        obj.hasMany = function($class, $getName, $localKey, $foreignKey, callback){
            var $getPath = obj.$getPath+$getName;
            var objList = new self.ListClass(obj, $class);

            objList.$localKey = $localKey;
            objList.$localKeyValue = obj[$localKey];
            objList.$foreignKey = $foreignKey;
            objList.$objParent = obj;
            objList.$getPath = $getPath;
            var classG = $class.call(self);
            var $classDataName = classG.$dataName;
            objList.$classDataName = $classDataName;
            obj.getObjData($classDataName, $getPath, $localKey, $foreignKey, true, function (data) {
                objList.loadList(data);
                if( callback != undefined )
                    callback(objList);
            });

            return objList;
        };


        obj.belongsTo = function($class, $getName, $localKey, $foreignKey, callback){
            var $getPath = obj.$getPath+$getName;
            var getObj = new $class();
            getObj.setParent(obj);
            getObj.setGetPath($getPath);
            var $classDataName = getObj.$dataName;
            obj.getObjData($classDataName, $getPath, $localKey, $foreignKey, false,function (data) {
                getObj.setAttr(data);
                getObj.onChange();
                if(callback != undefined)
                    callback(getObj);
            });
            return getObj;
        };


        obj.setGetPath = function(getPath){
            obj.$getPath = getPath;
        };


        obj.refrash = function(callback){
            App.webSocketHelper.getObj(obj.$getPath, {}, function (data) {
                obj.setAttr(data);
                if(callback !== undefined)
                    callback(obj);
            });
        };


        obj.save = function(callback){
            //App.webSocketHelper.saveObj(obj.$getPath, {}, obj.getAttr(), function(data) {
            //    callback(obj, data);
            //});

            var $attributes = obj.getAttr();
            App.webSocketHelper.saveObj(obj.$getPath, {}, $attributes, function (data) {
                callback(data);
            })
        };


        obj.delete = function(callback){
            App.webSocketHelper.getFn(obj.$getPath+'.delete()', {}, function (data) {
                if( typeof callback == 'function')
                    callback(data);
            })
        };


        obj.setAttr = function($attributes){
            for (var attrname in $attributes) {
                if( obj.$defattr.indexOf(attrname) != -1 || obj.$attributes.indexOf(attrname) != -1) {
                    obj[attrname] = "" + $attributes[attrname];
                    if (obj.inited == false && attrname == 'id') obj.onInit();
                }else{
                    //App.error(4012,'-= IS NOT VALID ARRT', { attrname:attrname, $attributes:$attributes[attrname], obj:obj});
                }
            }
        };


        obj.loadAttr = function(){
            obj.$attributes.forEach(function(item){
                obj[item] = null;
            });
        };


        obj.init = function($attributes){
            obj.loadAttr();
            obj.setAttr($attributes);
        };


        obj.getAttr = function(){
            var resAttr = {'id': obj.id};
            obj.$attributes.forEach(function(item){
                resAttr[item] = obj[item];
            });
            return resAttr;
        };


        obj.onInit = function(){
            obj.inited = true;
            //obj.runObserver();


            if( App.models[obj.$dataName] == undefined)
                App.models[obj.$dataName] = {};

            if( App.models[obj.$dataName][obj.id] == undefined)
                App.models[obj.$dataName][obj.id] = new Array();

            App.models[obj.$dataName][obj.id].push(obj);
        };


        return this;
    };
})
    .wait('models','load')
    .wait('protoClass','load')
    .waitingRun();