appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.CompanyClass = function($attributes){
            var obj = this;
            obj.__proto__ = new self.BasicClass(obj);

            obj.$dataName = 'Company';
            obj.$attributes = ['name', 'email', 'phone', 'county_id', 'default_lang_id', 'balanse'];


            obj.users = function(callback){
                return obj.hasMany(self.UserClass, '.users', 'id', 'company_id', callback)
            };


            obj.channels = function(callback){
                return obj.hasMany(self.ChannelClass, '.channels', 'id', 'company_id', callback)
            };


            obj.clientChannels = function(callback){
                return obj.hasMany(self.ClientChannelClass, '.clientChannels', 'id', 'company_id', callback)
            };


            obj.contacts = function(callback){
                return obj.hasMany(self.ContactClass, '.contacts', 'id', 'company_id', callback)
            };


            obj.createContact = function($attributes, callback){
                App.webSocketHelper.getFn(obj.$getPath+'.createContact($attributes)', {$attributes:$attributes}, function (data) {
                    self.setChangeCreate( {className:'Contact', objAttr:data});
                    if( callback !== undefined) callback(data);
                })
            };


            obj.quickReplys = function(callback){
                return obj.hasMany(self.QuickReplyClass, '.quickReplys', 'id', 'company_id', callback)
            };


            obj.createQuickReplys = function($attributes, callback){
                App.webSocketHelper.getFn(obj.$getPath+'.createQuickReplys($attributes)', {$attributes:$attributes}, function (data) {
                    self.setChangeCreate( {className:'QuickReply', objAttr:data});
                    if( callback !== undefined) callback(data);
                })
            };


            obj.createUser = function($attributes, callback){
                App.webSocketHelper.getFn(obj.$getPath+'.createUser($attributes)', {$attributes:$attributes}, function (data) {
                    self.setChangeCreate( {className:'User', objAttr:data});
                    if( callback !== undefined) callback(data);
                })
            };


            obj.createChannel = function($attributes, callback){
                App.webSocketHelper.getFn(obj.$getPath+'.createChannel($attributes)', {$attributes:$attributes}, function (data) {
                    self.setChangeCreate( {className:'Channel', objAttr:data});
                    if( callback !== undefined) callback(data);
                })
            };


            obj.tickets = function(callback){
                return obj.hasMany(self.TicketClass, '.tickets', 'id', 'company_id', callback)
            };


            obj.checkUserEmail = function(email, callback){
                App.webSocketHelper.getFn(obj.$getPath+'.checkUserEmail($email)', {$email:email}, function (data) {
                    if( callback !== undefined) callback(data);
                })
            };

            obj.getTicketCount = function(){
                var $resCount = 0;

                var $tickets = obj.getInitObj('tickets', obj.tickets);
                if( $tickets != null) {
                    $tickets.getListArr().forEach(function ($ticket) {
                        if ($ticket.status == 1) {
                            $lastMessgeStatus = $ticket.getLastMessageStatus();
                            if($lastMessgeStatus == 2)
                                $resCount++;
                        }
                    });
                }

                obj.$resCount = $resCount;
                return $resCount;
            };

            obj.getTicketTypeCount = function(){
                $counts = obj.getTicketCount();
                if( $counts == null || $counts == undefined || $counts == 0 )
                    return '';
                else
                    return $counts;
            };

            obj.init($attributes);
            return this;

    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();