appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.MessageWhatsappClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'MessageWhatsapp';
        obj.$attributes = ['message_id', 'status', 'out_message_id', 'out_message_sub_id', 't', 'from', 'notify', 'type'];

        obj.basicChannel = function(callback){
            return obj.belongsTo(self.MessageClass, '.basicMessage', 'message_id', 'id', callback);
        };

        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();