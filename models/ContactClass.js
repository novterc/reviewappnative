appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.ContactClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'Contact';
        obj.$attributes = ['company_id', 'name', 'bday', 'sex'];


        obj.company = function(callback){
            return obj.belongsTo(self.CompanyClass, '.company', 'company_id', 'id', callback);
        };


        obj.clientChannels = function(callback){
            return obj.hasMany(self.ClientChannelClass, '.clientChannels', 'id', 'contact_id', callback);
        };


        obj.createClientChannel = function($attributes, callback){
            App.webSocketHelper.getFn(obj.$getPath+'.createClientChannel($attributes)', {$attributes:$attributes}, function (data) {
                self.setChangeCreate( {className:'ClientChannel', objAttr:data});
                if( callback !== undefined) callback(data);
            })
        };


        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();