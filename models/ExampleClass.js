appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.ExampleClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'ExampleClass';
        obj.$attributes = ['name', 'email', 'phone', 'company_id'];

        obj.users = function(callback){
            return obj.hasMany(self.UserClass, '.users', 'id', 'company_id', callback)
        };

        obj.company = function(callback){
            return obj.belongsTo(self.CompanyClass, '.company', 'company_id', 'id', callback);
        };

        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();