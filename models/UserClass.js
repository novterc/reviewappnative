appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.UserClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'User';
        obj.$attributes = ['first_name', 'last_name', 'email', 'phone', 'company_id', 'lang_id'];


        obj.company = function(callback){
            return obj.belongsTo(self.CompanyClass, '.company', 'company_id', 'id', callback);
        };


        obj.changePassword = function(data, callback){
            App.webSocketHelper.getFn(obj.$getPath+'.changePassword($attributes)', {$attributes:data}, function (data) {
                if( callback !== undefined) callback(data);
            })
        };


        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();