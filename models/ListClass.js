appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.ListClass = function($objParent, $class){
        var obj = this;
        obj.__proto__ = new self.protoClass(obj);

        obj.setParent($objParent);
        obj.$listClass = $class;
        obj.$listItem = {};
        obj.inited = false;
        obj.$isList = true;


        obj.loadList = function(list){
            list.forEach(function(item){

                obj.addObj(item);
                //itemObj.addObserver(function(chObj){
                //    obj.changeList();
                //}, '*', 'update', false);
            });

            if( App.models['Lists'] == undefined)
                App.models['Lists'] = new Array();
            App.models['Lists'].push(obj);

            obj.inited = true;
            obj.onChange();
        };


        obj.foreach = function(callback){
            for (var itemKey in obj.$listItem) {
                var item = obj.$listItem[itemKey];
                callback(item)
            }
        };


        obj.listObserver = function(callback){
            obj.addObserver(callback);
            callback(obj);
        };


        obj.onChange = function(){
            if( self.checkAngular() == false ) {
                obj.$observers.forEach(function (item) {
                    item.callback(obj);
                });

                if (obj.$objParent != null)
                    obj.$objParent.onChange();
            }
        };


        obj.changeList = function(){
            obj.$observs.forEach(function(callback){
                callback(obj.getList());
            })
        };


        obj.find = function(id){
            return obj.$listItem[id];
        };

        obj.getList = function(){
            return obj.$listItem;
        };

        obj.getListArr = function(){
            var result = new Array();
            for (var itemKey in obj.$listItem) {
                result.push( obj.$listItem[itemKey] );
            }
            return result;
        };


        obj.count = function(){
            return App.getSize(obj.$listItem)
        };


        obj.getFirstItem = function(){
            var key = Object.keys(obj.$listItem)[0];
            return obj.$listItem[key];
        };

        obj.getLastItem = function(){
            var key = Object.keys(obj.$listItem)[Object.keys(obj.$listItem).length - 1]
            return obj.$listItem[key];
        };


        obj.getListAttr = function(){
            var resListAttr = {};
            for (var key in obj.$listItem) {
                resListAttr[key] = obj.$listItem[key].getAttr();
            }
            return resListAttr;
        };


        obj.addObj = function( data ){
            var itemObj = new obj.$listClass(data);
            itemObj.setParent(obj);
            itemObj.$getPath = obj.$getPath+'.find('+data.id+')';
            obj.$listItem[data.id] = itemObj;

            obj.onChange();
        };


        obj.delObj = function( objId ){
            delete obj.$listItem[objId];
            obj.onChange();
        };


        return this;
    };
})
    .wait('models','load')
    .wait('protoClass','load')
    .waitingRun();