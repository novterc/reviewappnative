appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.ChannelClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'Channel';
        obj.$attributes = ['type_id', 'priority', 'company_id', 'name', 'status'];


        obj.company = function(callback){
            return obj.belongsTo(self.CompanyClass, '.company', 'company_id', 'id', callback);
        };


        obj.messages = function(callback){
            return obj.hasMany(self.MessageClass, '.messages', 'id', 'channel_id', callback)
        };


        obj.getNick = function(){
            var ext = obj.getExtenderType();
            if( ext != false ) {
                return obj.getExtenderType().getNick();
            }
            else
                return '';
        };


        obj.getNumber = function(){
            var ext = obj.getExtenderType();
            if( ext != false ) {
                return obj.getExtenderType().getNumber();
            }
            else
                return '';
        };


        obj.getTypeName = function(){
            return App.lang.get('channelType',obj.type_id);
        };


        obj.getStatusName = function(){
            var statusId = obj.status;
            if( statusId != null ) {
                statusId = 0;
            }

            return App.lang.get('channelStatusClient', statusId);
        };


        obj.getStatus = function(){
            var ext = obj.getExtenderType();
            if( ext != false )
                return obj.getExtenderType().getStatus();
            else
                return 0;
        };


        obj.extenderType = function(callback){
            if(obj.inited == false ) return false;
            $extenderTypeClass = obj.getExtenderTypeClass();
            return obj.belongsTo($extenderTypeClass, '.extenderType', 'id', 'channel_id', callback);
        };


        obj.$extenderType = null;
        obj.getExtenderType = function(){
            if( obj.$extenderType==null)
                obj.$extenderType = obj.extenderType();
            else {
                if (obj.$extenderType.inited)
                    return obj.$extenderType;
            }
            return false;
        };


        obj.extenderTypeClassPath = {
            1: self.ChannelWhatsappClass,
            2: self.ChannelEmailClass,
        };


        obj.getExtenderTypeClass = function(){
            return obj.extenderTypeClassPath[obj.type_id]
        };


        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();