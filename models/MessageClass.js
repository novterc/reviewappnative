appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.MessageClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'Message';
        obj.$attributes = ['channel_id', 'type_id', 'direction', 'ticket_id', 'client_channel_id', 'text', 'type_message', 'src', 'title'];


        obj.channel = function(callback){
            return obj.belongsTo(self.ChannelClass, '.channel', 'channel_id', 'id', callback);
        };


        obj.clientChannel = function(callback){
            return obj.belongsTo(self.ClientChannelClass, '.clientChannel', 'client_channel_id', 'id', callback);
        };


        obj.getNotify = function(){
            if(obj.direction == 1) {
                var $channel = obj.getInitObj('channel', obj.channel, obj.channel_id);
                if ($channel != null)
                    return $channel.getNick();
            }
            if(obj.direction == 2) {
                var $clientChannel = obj.getInitObj('clientChannel', obj.clientChannel);
                if ($clientChannel != null)
                    return $clientChannel.notify;
            }
            return ''
        };


        obj.getText = function(){
            return obj.text.replace(/\n/g, "<br />");
        };



        obj.getTypeView = function(){
            return obj.type_message;
        };


        obj.extenderType = function(callback){
            if(obj.inited == false ) return false;
            $extenderTypeClass = obj.getExtenderTypeClass();
            return obj.belongsTo($extenderTypeClass, '.extenderType', 'id', 'message_id', callback);
        };


        obj.$extenderType = null;
        obj.getExtenderType = function(){
            if( obj.$extenderType==null)
                obj.$extenderType = obj.extenderType();
            else {
                if (obj.$extenderType.inited)
                    return obj.$extenderType;
            }
            return false;
        };


        obj.extenderTypeClassPath = {
            1: self.MessageWhatsappClass,
            2: self.MessageEmailClass,
        };


        obj.getExtenderTypeClass = function(){
            return obj.extenderTypeClassPath[obj.type_id]
        };



        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();