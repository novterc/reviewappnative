appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.QuickReplyClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'QuickReply';
        obj.$attributes = ['company_id', 'text'];


        obj.company = function(callback){
            return obj.belongsTo(self.CompanyClass, '.company', 'company_id', 'id', callback);
        };


        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();