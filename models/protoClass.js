appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.protoClass = function(obj){
        var obj = obj;
        obj.$childs = new Array();


        obj.setParent = function(objParent){
            obj.$objParent = objParent;
            obj.$objParent.addChild(obj)
        };


        obj.addChild = function(childObj){
            obj.$childs.push(childObj);
        };


        obj.treeDestroy = function(){
            obj.$childs.forEach(function(child){
                child.treeDestroy();
            });

            obj.destroy();
        };


        obj.destroy = function(){
            obj.$observers = new Array();
        };


        obj.runObserver = function(){
            var observerObj = obj;

            Object.observe(observerObj, function(changes){
                changes.forEach(function(change) {
                    if( obj.$observers.length != 0 ){
                        obj.$observers.forEach(function(item){
                            if( ( item.eventType === '*' || change.type == item.eventType) && ( item.attrName === '*' || change.name == item.attrName )){
                                item.callback(obj[item.attrName]);
                            }
                        })
                    }
                });
            });
        };


        obj.$observers = new Array();
        obj.addObserver = function(callback, attrName, type, run){
            if(type == undefined){
                var type = 'update';
            }
            if(attrName == undefined){
                var attrName = '*';
            }

            obj.$observers.push({
                'eventType':type,
                'attrName':attrName,
                'callback':callback,
            });

            if( run == undefined )
                callback(obj);
        };


        obj.onChange = function(){
            if( self.checkAngular() == false ) {
                obj.$observers.forEach(function (item) {
                    item.callback(obj);
                });
                if (obj.$objParent != null)
                    obj.$objParent.onChange();

            }
        };


        obj.dbl = function( attrName, ngModel){

            Object.observe(obj, function(changes){
                ngModel[attrName] = obj[attrName];
            });

            Object.observe(ngModel, function(changes){
                obj[attrName] = ngModel[attrName];
            })
        };


        obj.$getedObjs = {};
        obj.getInitObj = function(collectionName, methodName, id){
            if( obj.$getedObjs[collectionName] != undefined && obj.$getedObjs[collectionName][id] != undefined ){
                if( obj.$getedObjs[collectionName][id].inited ) {
                    return obj.$getedObjs[collectionName][id];
                }
            }else{
                obj.$getedObjs[collectionName] = {};
                obj.$getedObjs[collectionName][id] = methodName();
                //if( obj.$getedObjs[collectionName][id] != undefined )
                //    return obj.$getedObjs[collectionName][id];
            }
        };


        return this;
    };
})
    .wait('models','load')
    .waitingRun();