appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.TicketClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'Ticket';
        obj.$attributes = ['company_id', 'client_channel_id', 'channel_id', 'contact_id', 'user_id', 'status', 'priority', 'aside'];


        obj.company = function(callback){
            return obj.belongsTo(self.CompanyClass, '.company', 'company_id', 'id', callback);
        };


        obj.clientChannel = function(callback){
            return obj.belongsTo(self.ClientChannelClass, '.clientChannel', 'client_channel_id', 'id', callback);
        };


        obj.channel = function(callback){
            return obj.belongsTo(self.ChannelClass, '.channel', 'channel_id', 'id', callback)
        };


        obj.contact = function(callback){
            return obj.belongsTo(self.ContactClass, '.contact', 'contact_id', 'company_id', callback)
        };


        obj.messages = function(callback){
            return obj.hasMany(self.MessageClass, '.messages', 'id', 'ticket_id', callback)
        };


        obj.getCountMessage = function(){
            var $messages = obj.getInitObj('messages', obj.messages);
            if( $messages != null)
                return $messages.count();
            else
                return '';
        };


        obj.getLastTextMessage = function(){
            var $messages = obj.getInitObj('messages', obj.messages);
            if( $messages != null){
                var $lastMessage = $messages.getLastItem();
                if( $lastMessage != null)
                    return $lastMessage.text;
            }else
                return '';
        };


        obj.getFirstTextMessage = function(){
            var $messages = obj.getInitObj('messages', obj.messages);
            if( $messages != null){
                var $lastMessage = $messages.getFirstItem();
                if( $lastMessage != null)
                    return $lastMessage.text;
            }else
                return '';
        };


        obj.getContactNotify = function(){
            var $clientChannel = obj.getInitObj('clientChannel', obj.clientChannel, obj.client_channel_id);
            if($clientChannel != null )
                return $clientChannel.notify;
            else
                return '';
        };


        obj.getContactNumber = function(){
            var $clientChannel = obj.getInitObj('clientChannel', obj.clientChannel, obj.client_channel_id);
            if($clientChannel != null )
                return $clientChannel.number;
            else
                return '';
        };


        obj.createMessage = function($attributes, callback){
            App.webSocketHelper.getFn(obj.$getPath+'.createMessage($attributes)', {$attributes:$attributes}, function (data) {
                self.setChangeCreate( {className:'Message', objAttr:data});
                if( callback !== undefined) callback(data);
            })
        };


        obj.close = function($attributes, callback){
            App.webSocketHelper.getFn(obj.$getPath+'.close($attributes)', {$attributes:$attributes}, function (data) {
                if( callback !== undefined) callback(data);
            })
        };


        obj.getTimeDate = function(){
            return moment(obj.created_at, "YYYY-MM-DD hh:mm:ss").format("hh:mm DD.MM.YYYY");
        };


        obj.getTitleMessage = function(){
            var message = '';
            message = obj.getFirstTextMessage();

            if( typeof message  != 'string')
                message = '';

            if(message.length > 20)
                message = message.substring(0,20);

            return message;
        };


        obj.setAsideHour = function(countHour, callback){
            App.webSocketHelper.getFn(obj.$getPath+'.setAsideHour($countHour)', {$countHour:countHour}, function (data) {
                if( callback !== undefined) callback(data);
            })
        };

        obj.setAside = function(date, callback){
            var ut = (new Date(date).getTime()/1000);
            App.webSocketHelper.getFn(obj.$getPath+'.setAside($ut)', {$ut:ut}, function (data) {
                if( callback !== undefined) callback(data);
            })
        };

        obj.getLastMessageStatus = function(){
            var $messages = obj.getInitObj('messages', obj.messages);
            if( $messages != null){
                var $lastMessage = $messages.getLastItem();
                if( $lastMessage != null)
                    return $lastMessage.direction;
            }else
                return false;
        };

        obj.unixtime = function(){

        };



        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();