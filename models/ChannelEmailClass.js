appNative.createRelation(function(){
    var self = appNative.Models;
    var App = appNative;
    appNative.Models.ChannelEmailClass = function($attributes){
        var obj = this;
        obj.__proto__ = new self.BasicClass(obj);

        obj.$dataName = 'ChannelEmail';
        obj.$attributes = ['channel_id', 'email', 'password', 'nick', 'after', 'status'];


        obj.basicChannel = function(callback){
            return obj.belongsTo(self.ChannelClass, '.basicChannel', 'channel_id', 'id', callback);
        };


        obj.getNumber = function(){
            return obj.email;
        };


        obj.getNick = function(){
            return obj.nick;
        };


        obj.getStatus = function(){
            return obj.status;
        };


        obj.isVerify = function(){
            return false;
        };


        obj.init($attributes);
        return this;
    };
})
    .wait('models','load')
    .wait('BasicClass','load')
    .waitingRun();