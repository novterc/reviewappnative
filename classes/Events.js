
appNative.createRelation(function(){

    appNative.events.addTost('create', 'Ticket', undefined, 'Новый тикит' );

    appNative.relationModuleSetStatus('events', 'init');
}, 'modelsInit')
    .wait('protoClass','load')
    .wait('BasicClass','load')
    .wait('models','init')
    .waitingRun();

appNative.inClass('events',function (App, config) {
    this.Obj = {

        eventsList: {},


        event: function(){
            this.oldValueMask = undefined;
            this.newValueMask = undefined;
            this.callback = function(){};

            this.setCallback = function(newCallback){
                this.callback = newCallback;
            };

            return this;
        },


        addEvent: function(type, className, optionName, eventObj){
            if(self.eventsList[type] == undefined) self.eventsList[type] = {};
            if(self.eventsList[type][className] == undefined) self.eventsList[type][className] = {};
            if(self.eventsList[type][className][optionName] == undefined) self.eventsList[type][className][optionName] = new Array();
            self.eventsList[type][className][optionName].push(eventObj);
        },


        onEvent: function(type, className, newAttrValues, changes){
            function checkItem(item, oldValue, newValue){
                if (item.oldValueMask == undefined || self.checkMask(item.oldValueMask, oldValue)) {
                    if (item.newValueMask == undefined || self.checkMask(item.newValueMask, newValue)) {
                        item.callback(oldValue, newValue)
                    }
                }
            }

            if( changes != undefined && changes.names != undefined &&  changes.names instanceof Array ) {
                changes.names.forEach(function (changeName) {
                    if (
                        (self.eventsList[type] != undefined) &&
                        (self.eventsList[type][className] != undefined)

                    ) {
                        var oldValue = changes.originalsValue[changeName];
                        var newValue = newAttrValues[changeName];

                        if (self.eventsList[type][className][changeName] instanceof Array) {
                            var eventItem = self.eventsList[type][className][changeName];
                            eventItem.forEach(function (item) {
                                checkItem(item, oldValue, newValue);
                            });
                        }

                    }
                });
            };

            if(
                (self.eventsList[type] != undefined) &&
                (self.eventsList[type][className] != undefined) &&
                ( self.eventsList[type][className][undefined] instanceof Array )
            ) {
                var eventItem = self.eventsList[type][className][undefined];

                var oldValue = undefined;
                if( changes != undefined && changes.originalsValue != undefined ) oldValue = changes.originalsValue;

                eventItem.forEach(function (item) {
                    checkItem(item, oldValue, newAttrValues);
                });
            }


        },


        checkMask: function(mask, value){
            console.log('OOOOOopps this function not exists')
        },



        add: function(type, className, optionName, oldValueMask, newValueMask, callback ){
            var nEvent = new self.event();
            nEvent.setCallback(callback);
            self.addEvent(type, className, optionName, nEvent);
        },

        addTost: function(type, className, optionName, message){
            self.add(type, className, optionName, undefined, undefined, function(){
                App.view.tost(message);
            } );
        }


    };



    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});