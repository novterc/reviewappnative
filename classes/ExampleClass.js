appNative.inClass('example',function (App, config) { 
    this.Obj = {

    	testVal: 123,
    	testFunc: function(){
    		console.log(self.testVal);
    	},

    };
    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});