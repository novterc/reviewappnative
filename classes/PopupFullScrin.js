appNative.createRelation(function(){
    appNative.popupFullScrin.init()
}, 'popupFullScrin')
    .wait('popupFullScrin','load')
    .waitingRun();

appNative.inClass('popupFullScrin',function (App, config) {
    this.Obj = {

        domElem:$("#popup-fullscreen"),

        messageDomElem:$('#popup-fullscreen-message'),


        init: function(){
            App.relationModuleSetStatus('popupFullScrin', 'init');
        },

        show: function(){
            self.domElem.removeClass('hide');
        },


        hide: function(){
            self.domElem.addClass('hide');
        },


        setMessage: function(str){
            self.messageDomElem.text(str);
        },


        showMessage: function(str){
            self.setMessage(str);
            self.show();
        },


        hideMessage: function(str){
            self.setMessage(str);
            self.hide();
        }


    };
    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});