appNative.createRelation(function(){
    appNative.webSocketHelper.connecting()
}, 'webSocketHelper')
    .wait('webSocketHelper','load')
    .wait('popupFullScrin','init')
    .wait('angular','load')
    .waitingRun();

appNative.inClass('webSocketHelper',function (App, config) {
    var log = App.log;
    this.Obj = {

        config: {
            port: "9090",
            uri: window.location.host,
        },

        conn: false,
        connectedStatus: false,
        index: 0,
        isdieconnected: false,


        connecting: function () {
            App.popupFullScrin.showMessage('Connecting...');
            self.conn = new WebSocket('ws://' + self.config.uri + ':' + self.config.port);
            self.conn.onopen = self.onOpen;
            self.conn.onclose = self.onClose;
            self.conn.onmessage = self.onMessage;
            self.conn.onerror = self.onError;
        },


        onOpen: function (event) {
            log('webSocket connect open');
            self.connectedStatus = true;
            self.callbeck('onOpen', event);
            App.popupFullScrin.hideMessage('Connect');
            self.isdieconnected = true;
            App.relationModuleSetStatus('webSocketHelper', 'connect');
        },


        getStat: function(){
            return self.connectedStatus;
        },


        onClose: function (event) {
            log('webSocket connect close');
            self.connectedStatus = false;
            self.callbeck('onClose', event);
            App.popupFullScrin.showMessage('Disconnect');
            if(self.isdieconnected)
                location.reload();
            else
                self.connectTimeOut = setTimeout(self.connecting, 1000);
        },


        onMessage: function (event) {
            log('webSocket message', event);
            self.callbeck('onMessage', event);
            if(event.type == "message"){
                var jsonData = JSON.parse(event.data);

                log('webSocket message json', jsonData);
                if( jsonData.method != undefined ){
                    self[jsonData.method](jsonData.data, jsonData.requestId);
                }
            }
        },


        onError: function(event){
            log('webSocket connect error');
        },


        send: function(msgObj, requestId){
            if( requestId == undefined ) requestId = self.getRequestId();
            msgObj.requestId = requestId;

            App.log('webSocket send', msgObj);
            if(self.getStat()==true) {
                self.conn.send(JSON.stringify(msgObj));
            }else{
                self.addCallBack('onOpen',function(){
                    self.conn.send(JSON.stringify(msgObj));
                }, true)
            }
            return msgObj.requestId;
        },


        getRequestId: function(){
            self.index++;
            return new Date().getTime()+'.'+self.index;
        },


        setAppClose: function(){
            window.location.href = '/auth/';
        },


        setTest: function(data){
            self.callbeck('setTest', data);
            log(data);
        },

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        addCallBack: function(method, Fn, oneRun){
            if( self.callbacks[method] == undefined )
                self.callbacks[method] = new Array();

            if(oneRun==undefined)oneRun = false;

            return ( self.callbacks[method].push({ callback: Fn, oneRun: oneRun}) -1 );
        },


        delCallBack: function(method, num){
            self.callbacks[method][num] = false;
        },


        addRequest: function(method, Fn, requestId){
            var numCallback = self.addCallBack(method, function(data, responsId){
                if( responsId == requestId ){
                    self.delCallBack(method, numCallback);
                    Fn(data, responsId);
                }
            });
        },

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        callbacks: {},
        callbeck: function(method, data, requestId){
            //log('webSocket CallBack method -'+method);
            if( self.callbacks[method] != undefined ){
                self.callbacks[method].forEach(function(item, index) {
                    if(item != false){
                        item.callback(data, requestId);
                        if (item.oneRun == true) {
                            self.callbacks[method][index] = false;
                        }
                    }
                });
            }
        },


        getRequest: function(outObj, requestMethod, requestCallback, setRequestId){
            var requestId = self.getRequestId();
            //App.log(requestId);
            if( setRequestId != undefined && setRequestId == true )
                outObj.getData.requestId = requestId;
            self.addRequest(requestMethod, requestCallback, requestId);
            self.send(outObj, requestId);
        },

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        getObj: function(getPath, getData, callback){
            self.getRequest({
                method: 'getObj',
                getPath: getPath,
                getData: getData,
            }, 'setObj', callback );
        },


        setObj: function(data, requestId){
            self.callbeck('setObj', data, requestId);
        },


        getFn: function(getPath, getData, callback, setRequestId){
            self.getRequest({
                method: 'getFn',
                getPath: getPath,
                getData: getData,
            }, 'setFn', callback, setRequestId);
        },

        setFn: function(data, requestId){
            self.callbeck('setFn', data, requestId);
        },


        saveObj: function(getPath, getData, data, callback){
            self.getRequest({
                method: 'saveObj',
                getPath: getPath,
                getData: getData,
                data: data,
            }, 'saveObjResult', callback );
        },


        saveObjResult: function(data, requestId){
            self.callbeck('saveObjResult', data, requestId);
        },


        setChange: function(data){
            self.callbeck('setChange', data);
            //App.log(' wsh setChange data', data);
            App.Models.setChange(data);
        },

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    };
    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});