
appNative.createRelation(function(){
    appNative.relationModuleSetStatus('pageControllers', 'init');



}, 'modelsInit')
    .wait('indexPageControllers','load')
    .wait('usersPageControllers','load')
    .wait('channelPageControllers','load')
    .wait('clientsPageControllers','load')
    .wait('companyPageControllers','load')
    .wait('contactsPageControllers','load')
    .wait('profilePageControllers','load')
    .wait('quickReplyPageControllers','load')
    .wait('ticketsPageControllers','load')
    .waitingRun();


appNative.loadClass('indexPageControllers', '/js/appNative/pageControllers/index.js');
appNative.loadClass('usersPageControllers', '/js/appNative/pageControllers/users.js');
appNative.loadClass('channelPageControllers', '/js/appNative/pageControllers/channel.js');
appNative.loadClass('clientsPageControllers', '/js/appNative/pageControllers/clients.js');
appNative.loadClass('companyPageControllers', '/js/appNative/pageControllers/company.js');
appNative.loadClass('contactsPageControllers', '/js/appNative/pageControllers/contacts.js');
appNative.loadClass('profilePageControllers', '/js/appNative/pageControllers/profile.js');
appNative.loadClass('quickReplyPageControllers', '/js/appNative/pageControllers/quickReply.js');
appNative.loadClass('ticketsPageControllers', '/js/appNative/pageControllers/tickets.js');
appNative.loadClass('PageControllers', '/js/appNative/pageControllers/tickets.js');


appNative.inClass('pageControllers',function (App, config) {
    this.Obj = {

        'PageClass': function (obj) {
            var obj = obj;

            obj.$init = false;
            obj.$destroy = false;
            obj.$destroyObjs = new Array();


            obj.initDes = function () {
                obj.$init = false;
            };


            obj.initEn = function () {
                obj.$init = true;
            };


            obj.destroyAddObjs = function () {
                obj.$destroyObjs.forEach(function (item) {
                    item.treeDestroy();
                })
            };


            obj.addDestroyObj = function (destObj) {
                obj.$destroyObjs.push(destObj);
            };


            obj.protoInit = function (v1, v2, v3, v4, v5) {
                App.log('-=PAGE ' + obj.pageName + ' =-');
                obj.preInit(v1, v2, v3, v4, v5);
                if (obj.$init == false) {
                    obj.initEn();
                    obj.init(v1, v2, v3, v4, v5);
                }
                obj.postInit(v1, v2, v3, v4, v5);
                App.view.openPage(obj.pageName);
            };


            obj.protoDes = function () {
                if (obj.$destroy) {
                    obj.initDes();
                    obj.destroyAddObjs();
                    obj.destroy();
                }
            };


            obj.preInit = function () {
                //App.log('page preInit not overriding');
            };


            obj.postInit = function () {
                //App.log('page postInit not overriding');
            };


            obj.init = function () {
                //App.log('page init not overriding');
            };


            obj.destroy = function () {
                //App.log('page destroy not overriding');
            };


            return this;
        }

    };

    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});