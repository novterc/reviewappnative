appNative.createRelation(function(){
    appNative.view.init()
}, 'view')
    .wait('view','load')
    .wait('router','init')
    .wait('webSocketHelper','connect')
    .waitingRun();

appNative.inClass('view',function (App, config) {
    this.Obj = {

        pages:{},


        init: function(){
            $("body").on("click", "a", self.linkCallback);
            self.loadPageInView();

            window.history.replaceState({href:window.location.pathname}, null, null);
            window.addEventListener('popstate', function(e){
                //App.log('+',e);
                if(e.state != undefined && e.state.href != undefined )
                    App.router.nav(e.state.href);
            });

            appNative.router.nav(window.location.pathname);



            //$(window).bind('statechange',function(){
            //  alert('123');
            //});
        },


        loadPageInView: function (){
            $('#pages-wrapper>div').each(function(){
                var controllerName = $(this).attr('data-pageName');
                self.pages[controllerName] = $(this);
            });
        },


        openPage: function(pageName){
            if(self.pages[pageName]!=undefined){
                if( self.thisOpenPage != undefined )
                    self.hidePage(self.pages[self.thisOpenPage]);

                self.thisOpenPage = pageName;
                self.showPage(self.pages[pageName]);

                var pageTitlte = self.getData(self.pages[pageName], 'page-title');
                if( pageTitlte != '')
                self.pageTitle(pageTitlte);

                $('#nav-mobile').sideNav('hide');

            }else{
                App.error(1001,'page not found');
            }
        },


        hideAllPage: function(){
            var keys = Object.keys(self.pages);
            for(var i = keys.length; i--;){
                var domElem = self.pages[keys[i]];
                self.hidePage(domElem);
            }
        },


        hidePage: function(domElem){
            (function ($) {
                $(domElem).fadeOut(0);
            }(jQuery));
        },


        showPage: function(domElem){
            (function ($) {
                $(domElem).fadeIn(200);
            }(jQuery));
        },


        addHistory: function(href){
            history.pushState({href:href}, 'mTitly', href);
        },


        linkCallback: function(evt){
            var fHref = evt.currentTarget.href.replace(window.location.origin, '');
            if( self.openHref(fHref) ) {
                evt.preventDefault();
            }
        },

        openHref: function(href){
            var res = App.router.nav(href);
            if(res == true) {
                self.addHistory(href);
                return true;
            }
        },

        getData: function(domEl, name){
           return $(domEl).find('.appNative-data-wrapper[data-name="'+name+'"]').text();
        },

        pageTitle: function(title){
            $('#pageTitle').text(title);
        },

        tost: function(msg){
            Materialize.toast(msg, 4000)
        },


    };
    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});


function appNativeLink(event, obj){
    var href = $(obj).attr('data-href');
    console.log('appNativeLink - '+href);
    if( appNative.view.openHref(href)){
        event.preventDefault();
        event.stopPropagation();
    } else {
        console.log('openHref return false');
    }
    return false;
}