appNative.inClass('lang',function (App, config) {
    this.Obj = {

        langData: {},

        get: function(chankName, id){
            var part = self.langData[chankName];
            if(id == undefined )
                return part;
            else
                return part[id];
        },

        getArr: function(chankName, id){
            var data = self.get(chankName, id);
            var arr = [];
            for( var key in data ){
                arr.push({
                    key: key,
                    value: data[key],
                })
            }

            return arr;
        },


        loadJsonData: function(jsonStr){
            var data = JSON.parse(jsonStr);
             self.langData = data;
        }

    };

    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});