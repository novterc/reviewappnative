

appNative.createRelation(function(){
    appNative.relationModuleSetStatus('models', 'init');
}, 'modelsInit')
    .wait('protoClass','load')
    .wait('BasicClass','load')
    .wait('ListClass','load')
    .wait('CompanyClass','load')
    .wait('UserClass','load')
    .wait('ChannelClass','load')
    .wait('ChannelWhatsappClass','load')
    .wait('ChannelEmailClass','load')
    .wait('ClientChannelClass','load')
    .wait('ContactClass','load')
    .wait('QuickReplyClass','load')
    .wait('TicketClass','load')
    .wait('MessageClass','load')
    .waitingRun();


appNative.loadClass('protoClass', '/js/appNative/models/protoClass.js');
appNative.loadClass('BasicClass', '/js/appNative/models/BasicClass.js');
appNative.loadClass('ListClass', '/js/appNative/models/ListClass.js');
appNative.loadClass('CompanyClass', '/js/appNative/models/CompanyClass.js');
appNative.loadClass('UserClass', '/js/appNative/models/UserClass.js');
appNative.loadClass('ChannelClass', '/js/appNative/models/ChannelClass.js');
appNative.loadClass('ChannelWhatsappClass', '/js/appNative/models/ChannelWhatsappClass.js');
appNative.loadClass('ChannelEmailClass', '/js/appNative/models/ChannelEmailClass.js');
appNative.loadClass('ClientChannelClass', '/js/appNative/models/ClientChannelClass.js');
appNative.loadClass('ContactClass', '/js/appNative/models/ContactClass.js');
appNative.loadClass('QuickReplyClass', '/js/appNative/models/QuickReplyClass.js');
appNative.loadClass('TicketClass', '/js/appNative/models/TicketClass.js');
appNative.loadClass('MessageClass', '/js/appNative/models/MessageClass.js');
appNative.loadClass('MessageWhatsappClass', '/js/appNative/models/MessageWhatsappClass.js');


appNative.inClass('Models',function (App, config) {
    this.Obj = {


        setChange: function(data){

            App.log(' Models setChange data', data);
            var className = data.className+'Class';

            var changes = undefined
            if( data.changes != undefined ) changes =  data.changes;

            App.events.onEvent(data.type, data.className, data.objAttr, changes);

            if(data.type == "create"){
                self.setChangeCreate(data);
            }
            if(data.type == "update"){
                self.setDataObj(data.className, data.objAttr.id, data.objAttr);
                if( App.models[data.className] != undefined && App.models[data.className][data.objAttr.id] != undefined ){
                    App.models[data.className][data.objAttr.id].forEach(function(obj){
                        obj.setAttr(data.objAttr);
                        obj.onChange();
                    });

                    self.checkInList(data);
                }
            }
            if(data.type == "delete"){
                if(App.data[data.className][data.objAttr.id] != undefined){
                    delete App.data[data.className][data.objAttr.id];
                }
                self.delInLIst(data);
            }
        },


        setChangeCreate: function(data){
            App.log('-=setChangeCreate ', data);
            if( data.className == undefined ){ App.error(6012, 'Error setChangeCreate - className undefined', {data:data}); return false;}
            if( data.objAttr.id == undefined ){ App.error(6012, 'Error setChangeCreate - data.id undefined', {data:data}); return false;}
            self.setDataObj(data.className, data.objAttr.id, data.objAttr);
            self.addInList(data);
        },


        setDataObj: function($classDataName, $id, $obj){
            if( $classDataName == undefined || $classDataName == ''){ App.error(6012, 'Error setChangeCreate - className undefined', {$classDataName:$classDataName, $id:$id, $obj:$obj}); return false;}
            if( $id == undefined || $id == '' ){ App.error(6012, 'Error setChangeCreate - data.id undefined', {$classDataName:$classDataName, $id:$id, $obj:$obj}); return false;}

            if( App.data[$classDataName] == undefined) App.data[$classDataName] = {};
            App.data[$classDataName][$id] = $obj;
        },


        searchData: function( $dataName, $foreignKey, $value, $list){
            var reArr = new Array();
            for (var attrname in App.data[$dataName]) {
                if( App.data[$dataName][attrname][$foreignKey] == $value ){
                    if( $list == true)
                        reArr.push(App.data[$dataName][attrname]);
                    else
                        return App.data[$dataName][attrname];
                }
            }

            return reArr;
        },


        addInList: function(date){
            if( App.models.Lists != undefined)
            App.models.Lists.forEach(function(list){
                if( list.$classDataName == date.className ){
                    if( date.objAttr[list.$foreignKey] == list.$localKeyValue ){
                        list.addObj(date.objAttr);
                        list.onChange();
                    }
                }
            });
        },


        checkInList: function(data){
            if( App.models.Lists != undefined)
                App.models.Lists.forEach(function(list){
                    if( list.$classDataName == data.className ){
                        if( data.objAttr[list.$foreignKey] == list.$localKeyValue ){
                            if( list.$listItem[data.objAttr.id] == undefined ) {
                                list.addObj(data.objAttr);
                            }
                        }else{
                            if( list.$listItem[data.objAttr.id] != undefined ) {
                                list.delObj(data.objAttr.id);
                            }
                        }
                    }
                });
        },


        delInLIst: function(data){
            if( App.models.Lists != undefined)
            App.models.Lists.forEach(function(list){
                if( list.$classDataName == data.className ){
                    if( list.$listItem[data.objAttr.id] != undefined ){
                        delete list.$listItem[data.objAttr.id];
                        list.onChange();
                    }
                }
            });
        },


        checkAngular: function(){
            var isAngular = false;
            var e = new Error('dummy');
            var stack = e.stack.replace(/^[^\(]+?[\n$]/gm, '')
                .replace(/^\s+at\s+/gm, '')
                .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
                .split('\n');
            stack.forEach(function(item){
                if( item.indexOf('angular') != -1 ) {
                    //App.log('--- IS ANGULAR--');
                    isAngular = true;
                }
            });
            //App.log('onChangeStack', e, stack);

            return isAngular;
        },


        getRetryByDate: function(date){
            var ut = new Date(date).getTime();
            if( isNaN(ut)  ) ut = 0;
            var tut = new Date().getTime();
            var r = Math.ceil((ut-tut)/1000);
            if( r < 0 ) r = 1;
            return r;
        },



    };
    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});