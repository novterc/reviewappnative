appNative.createRelation(function(){
    appNative.router.init()
}, 'router')
    .wait('router','load')
    .wait('pageControllers','init')
    .wait('ThisUserClass','init')
    .waitingRun();

appNative.inClass('router',function (App, config) {
    this.Obj = {

        init: function (){
            self._routes = [];
            for( var route in self.routes ){
                var methodName = self.routes[route];
                self._routes.push({
                    methodName: methodName,
                    pattern: new RegExp('^'+route.replace(/:\w+/, '(\\w+)')+'$'),
                    callback: App.pageControllers[methodName],
                    href: methodName
                });
            }

            App.relationModuleSetStatus('router', 'init');
        },


        routes: {
            "/": "indexPage",
            "/profile": "profilePage",
            "/channels": "channelsPage",
            "/channel/:id": "channelPage",
            "/channelAdd": "channelAddPage",
            "/company": "companyPage",
            "/channelVerify/:id": "channelVerify",
            "/clients": "clientsPage",
            "/clientAdd": "clientAddPage",
            "/client/:id": "clientPage",
            "/contact/:id": "contactPage",
            "/contacts": "contactsPage",
            "/tickets": "ticketsPage",
            "/tickets/:type": "ticketsPage",
            "/ticket/:id":"ticketPage",
            "/quickReplys":"quickReplysPage",
            "/quickReply/:id":"quickReplyPage",
            "/users":"usersPage",
            "/user/:id":"userPage",
            "/userAdd":"userAddPage",
        },

        oldRoutes: false,


        nav: function (path){
            var i = self._routes.length;
            while (i--) {
                var args = path.match(self._routes[i].pattern);
                if (args) {
                    if( self.oldRoutes != false && typeof App.pageControllers[self.oldRoutes] == "object"){
                        App.pageControllers[self.oldRoutes].protoDes()
                    }
                    self.oldRoutes = self._routes[i].methodName;

                    if(typeof self._routes[i].callback == "object" ){
                        self._routes[i].callback.protoInit.apply(self, args.slice(1));
                    }else {
                        self._routes[i].callback.apply(self, args.slice(1));
                    }
                    self.selMenu(self._routes[i].href, args.slice(1));
                    return true;
                }
            }
        },


        selMenu: function(methodName, attributes){
            $('#nav-mobile li').each(function(){
                var domEl = this;
                var menuIds = $(this).attr('data-menu');

                    if( self.menuCheck(methodName, attributes, menuIds) ){
                        $(domEl).addClass('active');
                        self.munuSlide(domEl);
                    } else {
                        $(domEl).removeClass('active');
                    }

            });
        },

        menuCheck: function(methodName, attributes, menuIds){
            var result = false;
            if(menuIds != undefined) {
                var itemPageArr = menuIds.split(',');
                itemPageArr.forEach(function (item) {
                    var parse = item.split('/');
                    if (methodName == parse[0]) {
                        var attr = parse.slice(1);
                        if (attr.length == 0) {
                            result = true;
                        } else {
                            parse.slice(1).forEach(function (attrItem, key) {
                                if(attributes[key] == attrItem || ( attributes[key] == undefined && attrItem == "" ))
                                    result = true;
                            });
                        }
                    }
                });
            }
            return result;
        },


        munuSlide: function(domEl){
            if($(domEl).parents('.collapsible-body').size()!=0) {
                $(domEl).parents('.collapsible-body').slideDown(300);

                $('#nav-mobile .inmenu').not($(domEl).parents('.inmenu')).each(function () {
                    $(this).find('.collapsible-body').slideUp(300);
                });
            }else{
                if ($(domEl).find('.collapsible-body').size() != 0) {
                    $(domEl).find('.collapsible-body').slideDown(300);
                }

                $('#nav-mobile .inmenu').not(domEl).each(function () {
                    $(this).find('.collapsible-body').slideUp(300);
                });
            }
        },


    };
    this.Obj.config = App.merge_options(this.Obj.config, config);
    var self = this.Obj;
    return this.Obj;
});